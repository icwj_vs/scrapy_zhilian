# -*- coding: utf-8 -*-
import json
import re
from urllib.parse import quote

import scrapy
from zp.items import ZpItem


class ZhaopinSpider(scrapy.Spider):
    name = 'zhaopin'  #爬虫名
    allowed_domains = ['zhaopin.com', 'fe-api.zhaopin.com', 'jobs.zhaopin.com']  # 允许访问的域名
    start_urls = ['https://www.zhaopin.com/']  # 进口链接
    headers = {
        'Accept': 'application/json, text/plain, */*',
        'Cookie': 'sts_deviceid=164687402f24c0-06b1f8ff467eb1-382b5375-2073600-164687402f35f4; sts_sg=1; zp_src_url=https%3A%2F%2Fsou.zhaopin.com%2F%3Fjl%3D530; dywez=95841923.1530760797.2.2.dywecsr=sou.zhaopin.com|dyweccn=(referral)|dywecmd=referral|dywectr=undefined|dywecct=/; urlfrom=121126445; urlfrom2=121126445; adfcid=none; adfcid2=none; adfbid=0; adfbid2=0; dywea=95841923.1820455542925615900.1530250148.1530250148.1530760797.2; dywec=95841923; dyweb=95841923.13.10.1530760797; sts_evtseq=53; sts_sid=164687403c66d3-03869bff509b08-382b5375-2073600-164687403c76c8; ZP_OLD_FLAG=false; LastCity=%E5%85%A8%E5%9B%BD; LastCity%5Fid=489; GUID=7d66a2c154da4ad2926c67b448c1d625; ZL_REPORT_GLOBAL={%22sou%22:{%22actionIdFromSou%22:%223b084131-8512-4950-8d40-be5d16c32c5f-sou%22%2C%22funczone%22:%22smart_matching%22}}',
        'Host': 'fe-api.zhaopin.com',
        'Origin': 'https://sou.zhaopin.com',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.63 Safari/537.36 Qiyu/2.1.1.1'
    }
    job_headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Host': 'jobs.zhaopin.com',
        'cookie': 'dywez=95841923.1528648715.1.1.dywecsr=(direct)|dyweccn=(direct)|dywecmd=(none)|dywectr=undefined; JSSearchModel=0; LastSearchHistory=%7b%22Id%22%3a%22f61419c8-93e7-4541-8e17-a3adbbf222e2%22%2c%22Name%22%3a%22%e5%85%a8%e5%9b%bd+%2b+%e9%94%80%e5%94%ae%e7%ae%a1%e7%90%86+%2b+%e9%94%80%e5%94%ae%e6%80%bb%e7%9b%91%22%2c%22SearchUrl%22%3a%22http%3a%2f%2fsou.zhaopin.com%2fjobs%2fsearchresult.ashx%3fjl%3d489%26bj%3d7001000%26sj%3d000%22%2c%22SaveTime%22%3a%22%5c%2fDate(1529757964856%2b0800)%5c%2f%22%7d; hiddenEpinDiv=none; ZP_OLD_FLAG=false; LastCity=%E5%85%A8%E5%9B%BD; LastCity%5Fid=489; sts_deviceid=1647c1e485464-085529673-3f265972-2073600-1647c1e4855a4; urlfrom=121126445; urlfrom2=121126445; adfcid=none; adfcid2=none; adfbid=0; adfbid2=0; dywea=95841923.605513448409895300.1528648715.1531090598.1531092734.5; dywec=95841923; dyweb=95841923.2.10.1531092734; sts_sg=1; sts_evtseq=2; sts_sid=1647c3d12180-04464dd72-3f265972-2073600-1647c3d12195; referrerUrl=; stayTimeCookie=1531092736479',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.63 Safari/537.36 Qiyu/2.1.1.1'
    }
    def parse(self, response):
        sel=scrapy.Selector(response)
        div_list = sel.xpath('//div[@class="zp-jobNavigater-popContainer"]')
        for div_item in div_list:
            zwlb_big = div_item.xpath('div[@class="zp-jobNavigater-pop-title"]/text()').extract_first()
            for zwlb in div_item.xpath('div[@class="zp-jobNavigater-pop-list"]/a/text()').extract():
                url = 'https://fe-api.zhaopin.com/c/i/sou?start=0&pageSize=60&cityId=489&workExperience=-1&education=-1&companyType=-1&employmentType=-1&jobWelfareTag=-1&kw={}&kt=3'.format(
                    quote(zwlb))
                yield scrapy.Request(url=url, callback=self.parse_list, dont_filter=True,
                                     meta={'zwlb_big': zwlb_big, 'zwlb': zwlb, 'p': 1, 'size': 60, 'start': 60},
                                     headers=self.headers)

    #通过列表页面获取详细页面链接，并完成分页处理
    def parse_list(self,response):
        js = json.loads(response.body_as_unicode())
        self.job_headers['referer'] = 'https://sou.zhaopin.com/?jl=489&kw={}&kt=3'.format(quote(response.meta['zwlb']))
        for i in js['data']['results']:
            job = {}
            zwmc = i['jobName']  # zwmc
            zwlb_big = i['jobType']['items'][0]['name']  # zwlb_big
            zwlb = i['jobType']['items'][1]['name']  # zwlb
            gsmc = i['company']['name']  # gsmc
            gsgm = i['company']['size']['name']  # gsgm
            gsxz = i['company']['type']['name']  # gsxz
            gzjy = i['workingExp']['name']  # gzjy
            zdxl = i['eduLevel']['name']  # zdxl
            zwyx = i['salary']
            job['emplType'] = i['emplType']
            gzdd = i['city']['items'][0]['name']
            fbrq = i['updateDate']
            flxx = i['welfare']

<<<<<<< .merge_file_a88188
    #4.完成详细页面数据获取
    def parse_info(self,response):
        sel=scrapy.Selector(response)
        #1.职位名称
        zwmc=sel.xpath('//div[@class="top-fixed-box"]/div[@class="fixed-inner-box"]/div[1]/h1/text()').extract_first()
        #2.公司名称
        gsmc = sel.xpath('//div[@class="top-fixed-box"]/div[@class="fixed-inner-box"]/div[1]/h2/a/text()').extract_first()
        #3.福利信息
        flxx=sel.xpath('//div[@class="top-fixed-box"]/div[@class="fixed-inner-box"]/div[1]/div/span/text()').extract()
        #获取ul下的li的span和strong
        ul_li = response.css('ul.terminal-ul > li')
        info_dict = {}
        for item in ul_li:
            #decode 解码为unicode
            span_one=item.xpath('span/text()').extract_first()
            if span_one:
                span_one=span_one.strip("：")
            else:
                continue
            #string 表示获取strong下的所有文本，不管下面的节点包含关系
            #会有缺点，他会保留所有的空格
            strong_one = item.xpath('string(strong)').extract_first()
            #对strong_one的文本中的空格进行处理
            strong_list=[one.strip() for one in strong_one.split()]
            strong_one=''.join(strong_list)
            info_dict[span_one]=strong_one
        #4.职位月薪
        zwyx=info_dict.get('职位月薪','').strip('元/月')
        if zwyx=='面议':
            zwyx=''
        #分割月薪数据
        zwyx_list=zwyx.split('-')
        #判断是否分割成两部分了，如果可以，则最低和最高分别赋值给两个变量，如果不可以，则最低和最高赋值为zwyx的值
        if len(zwyx_list)==2:
            min_zwyx=zwyx_list[0]
            max_zwyx=zwyx_list[1]
        else:
            min_zwyx=max_zwyx = zwyx
        #5.工作地点
        gzdd=info_dict.get('工作地点','')
        #6.发布时间
        fbrq=info_dict.get('发布日期','')
        #7.公司性质
        gsxz=info_dict.get('公司性质', '')
        #8.工作经验
        gzjy=info_dict.get('工作经验', '')
        #9.最低学历
        zdxl=info_dict.get('最低学历', '')
        #10.招聘人数
        zprs=info_dict.get('招聘人数', '').strip('人')
        #11.职位类别
        zwlb=info_dict.get('职位类别', '')
        #12.公司规模
        gsgm=info_dict.get('公司规模', '')
        #13.公司行业
        gshy=info_dict.get('公司行业', '')
        #**14.公司主页、公司地点 。。。。。
        #获取岗位职责和任职要求
        text_xpath = sel.xpath('string(//div[@class="tab-inner-cont"][1])').extract()
        #提取岗位职责和任职要求
        #使用正则表达式
        com=re.compile('(要求|职责)[：:]?(.*?)(任职要求|职位描述|工作职责)[：:](.*?)工作地址：',re.S)
        re_list=re.findall(com,text_xpath[0])
        #岗位职责和任职要求
        if re_list:
            gwzz=re_list[0][1].strip()
            rzyq=re_list[0][3].strip()
        else:
            com = re.compile('(.*?)工作地址：', re.S)
            re_list = re.findall(com, text_xpath[0])
            if re_list:
                rzyq=re_list[0].strip()
            else:
                rzyq=''
            gwzz=''
        item_one=ZpItem()
        if zwmc:
=======
            pattern = re.compile('(\d+)K-(\d+)K').findall(zwyx)
            if pattern:
                min_zwyx = pattern[0][0] + '000'
                max_zwyx = pattern[0][1] + '000'
            else:
                min_zwyx = max_zwyx = 0

            item_one = ZpItem()
>>>>>>> .merge_file_a85332
            item_one['zwmc']=zwmc
            item_one['gsmc'] = gsmc
            item_one['flxx'] = flxx
            item_one['min_zwyx'] = min_zwyx
            item_one['max_zwyx'] = max_zwyx
            item_one['gzdd'] = gzdd
            item_one['fbrq'] = fbrq
            item_one['gsxz'] = gsxz
            item_one['gzjy'] = gzjy
            item_one['zdxl'] = zdxl
            item_one['zwlb'] = zwlb
            item_one['gsgm'] = gsgm
            item_one['zwlb_big'] = zwlb_big
            url = i['positionURL']
            if url:
                yield scrapy.Request(url=url, callback=self.parse_info, dont_filter=True, meta={'item': item_one},
                                     headers=self.job_headers)

        num = js['data']['numFound']
        if num > response.meta['start']:
            p = response.meta['p'] + 1
            url2 = 'https://fe-api.zhaopin.com/c/i/sou?start={0}&pageSize={1}&cityId=489&workExperience=-1&education=-1&companyType=-1&employmentType=-1&jobWelfareTag=-1&kw={2}&kt=3'.format(
                response.meta['start'], response.meta['size'], response.meta['zwlb'])
            start = response.meta['start'] + 60
            yield scrapy.Request(url=url2, callback=self.parse_list, dont_filter=True,
                                 meta={'zwlb_big': response.meta['zwlb_big'], 'zwlb': response.meta['zwlb'], 'p': p,
                                       'size': response.meta['size'], 'start': start},
                                 headers=self.headers)

    # 职位详细信息
    def parse_info(self, response):
        item = response.meta['item']
        ul_li = response.css('ul.terminal-ul li')
        info_dict = {}
        for li_item in ul_li:
            # decode 解码为unicode
            span_one = li_item.xpath('span/text()').extract_first().strip("：")
            # string 表示获取strong下的所有文本，不管下面的节点包含关系
            # 会有缺点，他会保留所有的空格
            strong_one = li_item.xpath('string(strong)').extract_first()
            # 对strong_one的文本中的空格进行处理
            strong_list = [one.strip() for one in strong_one.split()]
            strong_one = ''.join(strong_list)
            info_dict[span_one] = strong_one
        item['gshy'] = info_dict.get('公司行业', '')
        item['href'] = response.url
        item['rzyq'] = response.xpath('string(//div[@class="pos-ul"]|//div[@class="tab-inner-cont"])').extract_first()
        return item
